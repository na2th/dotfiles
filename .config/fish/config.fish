set -x PATH $HOME/.local/bin $PATH $HOME/toolchain/bin

alias vim="nvim"
alias print="lpr -o fit-to-page -o ppi=150"
