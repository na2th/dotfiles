set sw=2
set iskeyword+=:
set iskeyword+=-

" Latex suite configurations
let g:tex_flavor = 'latex'
let g:vimtex_view_method = 'zathura'
let g:vimtex_indent_enabled = 0
