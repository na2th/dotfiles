set nu rnu

set expandtab
set shiftwidth=4
set softtabstop=4
set tabstop=8
set autoindent cindent

filetype plugin on
filetype indent on
syntax enable

command! MakeTags !ctags -R .

augroup GAMS
	au!
	au BufNewFile,BufRead *.gms set syntax=gams
augroup END

call plug#begin("~/.local/share/vim/plugged")
Plug 'lervag/vimtex'
Plug 'tpope/vim-fugitive'
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'tomtom/tlib_vim'
Plug 'garbas/vim-snipmate'
Plug 'honza/vim-snippets'
Plug 'morhetz/gruvbox'
call plug#end()

colors gruvbox

set statusline=(%2n)%f%m%r\ %{FugitiveStatusline()}
set statusline+=%=%h%w[%{getcwd()}][%{&ff}]%y[%p%%][%-4l,%-4v][%L]

nnoremap <leader>ev :vsplit $HOME/.dotfiles<cr>
nnoremap <leader>sv :!cd $HOME/.dotfiles && ./sync<cr>
nnoremap <leader>rv :source $MYVIMRC<cr>

set spellfile=~/.vim/spell/en.utf-8.add
set spell
set textwidth=70
set colorcolumn=+1

set exrc
set secure
