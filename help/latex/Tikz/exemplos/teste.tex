\documentclass[10pt, a4paper, reqno]{amsart}
\usepackage[utf8]{inputenc}

\usepackage{bbm,amsmath,amsthm,mathtools,booktabs,bm,color,setspace,url}

%% Hyperlinks packages
\usepackage[%
  pdftex,% For direct pdf compilation compatibility
  plainpages=false,% Usage of not only arabic numbers
  pdfpagelabels,% Enable page labels
  colorlinks,% Colored links
  citecolor=black,% Citation color
  linkcolor=black,% Link color
  urlcolor=black,% URL color
  filecolor=black,% File color
  bookmarksopen% For starting document with bookmarks tree opened
]{hyperref} %For pdf links

\usepackage{tikz}
\usetikzlibrary{calc,positioning,backgrounds,}
\usepackage{pgfplots}
\pgfplotsset{compat=1.13}

\pgfplotsset{
    % Closed and open dots to represent discontinuities
    closed dot/.style = {
        only marks,
        mark = *,
        mark size = 1.5pt,
    },
    open dot/.style = {
        only marks,
        mark = *,
        fill = white,
        mark size = 1.5pt,
    },
}

\begin{document}

\begin{figure}
    \centering
    \begin{tikzpicture}
        \pgfmathsetmacro{\Mu}{4}
        \pgfmathsetmacro{\N}{2}
        \draw[gray, thin] (-\Mu - 1, 0) -- (\Mu + 1, 0);

        % Sphere ||x|| = mu
        \draw[dashed] (\Mu, 0) arc [radius = \Mu, start angle = 0, end angle = 180];
        \draw[|<->|] (-\Mu, 0) -- node [below] {$\mu$} (0, 0);

        % Sphere of radius N around the polytope
        \draw[dashed] (0, \Mu) circle [radius = \N];
        \draw[|<->|] (-\N, \Mu) -- node [above] {$N$} (0, \Mu);

        % K + mu v
        \coordinate (A) at (0.6, \Mu + 1.0);
        \coordinate (B) at (1.6, \Mu + 0.8);
        \coordinate (C) at (1.8, \Mu + 0.4);
        \coordinate (D) at (0.8, \Mu + 0.4);
        \draw [thick, blue, fill = blue!40] (A) -- (B) -- (C) -- (D) -- cycle;
        \node [blue] at (\N + 0.5, \Mu + 0.5) {$K + \mu v$};

        % K'
        \draw [blue, line width = 2pt, yshift = 1pt]
            (A |- 0, 0) -- node [above] {$K'$} (C |- 0, 0);

        % mu S(K + mu v)
        \pgfmathanglebetweenpoints{\pgfpointorigin}{\pgfpointanchor{C}{center}}
        \edef\startangle{\pgfmathresult}
        \pgfmathanglebetweenpoints{\pgfpointorigin}{\pgfpointanchor{A}{center}}
        \edef\endangle{\pgfmathresult}
        \draw [very thick, red] (\startangle:\Mu)
            coordinate (S-right)
            arc [radius = \Mu, start angle = \startangle, end angle = \endangle]
            coordinate (S-left);
        \node [red, right = 0.5] at (S-right) {$\mu S(K + \mu v)$};

        % K_mu
        \draw [green!30!black, line width = 2pt, yshift = -1pt]
            (S-left |- 0, 0) -- node [below] {$K_\mu$} (S-right |- 0, 0);

        \begin{scope}[very thin]
            % Lines from the origin to K
            \draw [red] (0, 0) -- (A);
            \draw [red] (0, 0) -- (C);

            % Vertical lines corresponding to the projections
            \draw [blue] (A) -- (A |- 0, 0);
            \draw [blue] (C) -- (C |- 0, 0);
            \draw [green!30!black] (S-left) -- (S-left |- 0, 0);
            \draw [green!30!black] (S-right) -- (S-right |- 0, 0);
        \end{scope}
    \end{tikzpicture}
\end{figure}

\bigskip

\begin{figure}
    \begin{tikzpicture}
        % The polytope
        \def\polytope{(1.5, -0.5) -- ++(-2, 2) -- ++(2, 2) -- ++(2, -2) -- cycle}
        \filldraw[blue, thick, fill = blue!40] \polytope;
        \node [blue!50!black] at (1.5, 1.5) {$s_0 P$};

        % Scaled polytope
        \draw[scale = 1.15, green!30!black] \polytope;
        \node[green!30!black] at (4, 0.5) {$(s_0+\varepsilon) P$};

        % Facet F
        \draw[very thick, red] (1.5, -.5) -- ++(-2, 2)
            node[below=0.2cm] {$F$};

        % Lattice
        \begin{pgfonlayer}{background}
            \draw[lightgray] (-1, 0) -- (4, 0)
                        (0, -1) -- (0, 4);
        \end{pgfonlayer}{background}
        \foreach \x in {-1, ..., 4}
            \foreach \y in {-1, ..., 4}
                \fill (\x, \y) circle [radius = 1pt];
        \node [below left] at (0, 0) {$0$};

        % Integral point x_0
        \fill (1, 0) circle [radius=2pt]
            node [below left] {$x_0$};
    \end{tikzpicture}
\end{figure}

\bigskip

\begin{figure}
    \centering
    \newcommand{\drawscaledsquare}[2]{
        % #1: scale factor
        % #2: label printed inside
        \filldraw [blue, line width = 1pt, fill = blue!40]
            (#1 * 2/3, 0) -- (#1, 0) -- (#1, #1 * 1/3) -- (#1 * 2/3, #1 * 1/3) -- cycle;

        \node at (#1 * 5/6, #1 * 1/6) {#2};
    }
    \begin{tikzpicture}[scale = 1.5]
        \draw (-1, 0) -- (4, 0);
        \draw (0, -0.5) -- (0, 2);

        \foreach \x in {-1, ..., 4}
            \foreach \y in {0, 1, 2}
                \fill[gray] (\x, \y) circle [radius = 1pt];

        \drawscaledsquare{1}{$P$};
        \drawscaledsquare{1.5}{$\frac32 P$};
        \drawscaledsquare{3}{$3P$};
    \end{tikzpicture}

    \bigskip
    
    \centering
    \begin{tikzpicture}
        \begin{axis}[
            axis lines = center,
            xmin = 0,
            xmax = 3.5,
            ymin = 0,
            ymax = 4.5,
        ]
            \draw [thick]
                (0, 0) -- (1, 0)
                (1, 1) -- (1.5, 1)
                (1.5, 0) -- (2, 0)
                (2, 1) -- (3, 1)
                (3, 2) -- (3.5, 2)
                ;

            \draw [dashed]
                (1, 0) -- (1, 1)
                (1.5, 1) -- (1.5, 0)
                (2, 0) -- (2, 1)
                (3, 1) -- (3, 4)
                ;

            \addplot [closed dot] coordinates {
                (0, 1) (1, 1) (1.5, 1) (2, 1) (3, 4)
            };
            \addplot [open dot] coordinates {
                (0, 0) (1, 0) (1.5, 0) (2, 0) (3, 1) (3, 2)
            };
        \end{axis}
    \end{tikzpicture}
\end{figure}

\bigskip

\begin{figure}
\begin{tikzpicture}[scale = .7]
\def \r{1.7} \def \R{3} \def \s{0.9}

\coordinate (a) at (90: \r);
\coordinate (b) at (90-72: \r);
\coordinate (c) at (90-2*72: \r);
\coordinate (d) at (90-3*72: \r);
\coordinate (e) at (90-4*72: \r);
\coordinate (A) at (90: \R);
\coordinate (B) at (90-72: \R);
\coordinate (C) at (90-2*72: \R);
\coordinate (D) at (90-3*72: \R);
\coordinate (E) at (90-4*72: \R);

\draw (A) -- (B) -- (C) -- (D) -- (E) -- cycle;
\draw (d) -- (a) -- (c) -- (e) -- (b) -- cycle;
\draw (a) -- (A) (b) -- (B) (c) -- (C) (d) -- (D) (e) -- (E);
  
\foreach \p in {(a),(b),(c),(d),(e),(A),(B),(C),(D),(E)}
  \filldraw[fill = white!60!black] \p circle [radius = 5pt];
\end{tikzpicture}
\end{figure}

\begin{figure}
 \begin{tikzpicture}
 \draw[gray!60, step = 0.25, very thin] (0,0) grid (10,6);
 \draw[gray] (0,0) grid (10,6);
 \draw[color=red, thick] (0,0) .. controls (2,0) and (0,5) .. +(6,5);
 \fill[gray!10!white, draw=black] (5,1) node (a) {} -- (8,2) node (b) {} -- (6,4) node (c) {} -- cycle;
 \fill[gray!20!white, draw=black] (a.center) -- (4,2.3) node (d) {} -- (c.center) -- cycle;
 \draw[dashed] (d) -- (b);
 \end{tikzpicture}
\end{figure}

\begin{figure}
 \begin{tikzpicture}
  \draw (0,0) grid (8,6);
  \draw node (a) at (4,3) {};
  \draw (a) circle (1);
  \draw[color=blue, rotate=30] (a) ellipse (3 and 1);
 \end{tikzpicture}
\end{figure}

\begin{figure}
 \begin{tikzpicture}
 \draw[line width = 0.2pt, step=0.5] (-2.5,-2.5) grid (-1,2.5);
 \draw[line width = 0.3pt, step=0.5] (-1,-2.5) grid (1,2.5);
 \draw[line width = 0.4pt, step=0.5] (1,-2.5) grid (2.5,2.5);
  \draw[color=blue, thick] (-2,0) -- (2,0) (0,-2) -- (0,2);
  \draw[color=red, fill=lightgray] (0:1) -- (50:1) -- (100:1) -- (150:1) -- ++(200:1) -- +(250:1) -- cycle;
 \end{tikzpicture}
\end{figure}


\begin{figure}
\begin{tikzpicture}
\def \w{ 12}
\def \h{ 6}

\draw[gray!60, step = 0.2, very thin] (0,0) grid (\w,\h);
\draw[gray, thin] (0,0) grid (\w,\h);
\foreach \x in {0,...,\w} {\node[below] at (\x, 0) {\x};};
\foreach \x in {0,...,\h} {\node[left] at (0, \x) {\x};};
\end{tikzpicture}
\end{figure}

\end{document}

